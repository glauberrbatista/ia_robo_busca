import random
import time
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from treelib import Node, Tree
import interface as iface
import heapq
from math import sqrt

valores = {
	0 : 1,
	1 : 5,
	2 : 10,
	3 : 15
}

cores = {
	0 : QColor(0,0,0), #preto - caminho
	1 : QColor(111, 0, 140), #roxo - visitados
	2 : QColor(226, 24, 203) #rosa - borda
}

class FilaPrioridade:
	def __init__(self):
		self.elements = []
	
	def empty(self):
		return len(self.elements) == 0
	
	def put(self, item, priority):
		heapq.heappush(self.elements, (priority, item))
	
	def get(self):
		return heapq.heappop(self.elements)[1]

class Robo(object):

	def __init__(self, pos_inicial, pos_final, terreno, qt_app, qt_tabela):
		super(Robo, self).__init__()
		self.pos_inicial = self.pos = pos_inicial
		self.pos_final = pos_final
		self.max_linhas = len(terreno)
		self.max_colunas = len(terreno[0])
		self.terreno = terreno
		self.visitados = []
		self.expandidos = []

		#Arvore auxiliar para expansao
		self.arvore = None

		#Variaveis do Qt
		self.qt_app = qt_app
		self.qt_tabela = qt_tabela
		#Caneta e role para pintar a borda
		self.MyBorderRole = 33
		self.pen = QPen(cores[0])
		self.pen.setWidth(2)
		self.delegate = iface.BorderItemDelegate(self.qt_tabela, self.MyBorderRole)

	def pega_posicao(self, pos):
		posicoes = {
			0 : [(self.pos[0] - 1), (self.pos[1])    ], #norte
			1 : [(self.pos[0])    , (self.pos[1] + 1)], #leste
			2 : [(self.pos[0] + 1), (self.pos[1])    ], #sul
			3 : [(self.pos[0])    , (self.pos[1] - 1)]  #oeste
		}

		return posicoes[pos]

	def expandir(self, nodo, profundidade=None):
		posicoes = {
			0 : [(self.pos[0] - 1), (self.pos[1])    ], #norte
			1 : [(self.pos[0])    , (self.pos[1] + 1)], #leste
			2 : [(self.pos[0] + 1), (self.pos[1])    ], #sul
			3 : [(self.pos[0])    , (self.pos[1] - 1)]  #oeste
		}
		if self.arvore.depth(nodo) == profundidade:
			return
		
		#nodo.tag = self.pos
		for i in range(0,4):
			novo_nodo = Node()
			if posicoes[i][0] >= 0 and posicoes[i][0] < self.max_linhas and posicoes[i][1] >= 0 and posicoes[i][1] < self.max_colunas and posicoes[i] not in self.expandidos:
			#if posicoes[i][0] >= 0 and posicoes[i][0] < self.max_linhas and posicoes[i][1] >= 0 and posicoes[i][1] < self.max_colunas :
				if self.arvore.get_node("%s" % posicoes[i]) == None:
					novo_nodo.identifier = "%s" % posicoes[i]
					novo_nodo.tag = posicoes[i]
					novo_nodo.data = self.terreno[posicoes[i][0]][posicoes[i][1]]
					self.qt_tabela.item(*posicoes[i]).setForeground(cores[2])
					self.qt_tabela.item(*posicoes[i]).setText(QString(u"\u25E6"))
					self.qt_app.processEvents()
					self.arvore.add_node(novo_nodo, nodo.identifier)

	def busca_largura(self):
		self.arvore = Tree()
		nodo_atual = Node()
		nodo_atual.data = self.terreno[self.pos[0]][self.pos[1]]
		nodo_atual.tag = self.pos_inicial
		nodo_atual.identifier = "%s" % self.pos_inicial
		self.arvore.add_node(nodo_atual)
		
		self.expandidos = []

		backtrack = []
		fila = []
		fila.append(nodo_atual)
		custo = 0
		encontrado = False

		while not encontrado:
			u = fila.pop(0)
			self.pos = u.tag
			self.qt_tabela.item(*self.pos).setForeground(cores[1])
			self.qt_tabela.item(*self.pos).setText(QString(u"\u2022"))
			self.qt_app.processEvents()
			self.expandidos.append(u.tag)
			self.expandir(u)

			for w in u.fpointer:
				fila.append(self.arvore.get_node(w))

			if self.pos == self.pos_final:
				encontrado = True

		final = self.arvore.get_node("%s" % self.pos_final)
		while final.bpointer:
			backtrack.append(final)
			final = self.arvore.get_node(final.bpointer)
		backtrack.append(final)

		for i in reversed(backtrack):
			custo += valores[int(self.terreno[i.tag[0]][i.tag[1]])]
			self.qt_tabela.item(*i.tag).setData(self.MyBorderRole, self.pen) 
			self.qt_tabela.setItemDelegate(self.delegate)
			self.qt_app.processEvents()
			time.sleep(0.1)

		#Fim da execucao
		print "------------------------"
		print "- Custo: %d" % custo
		print "- Nodos visitados: %d" % len(self.expandidos)
		print "- Tamanho Caminho: %d" % len(backtrack)
		print "------------------------"

	

	def busca_custo_uniforme(self):		
		self.arvore = Tree()
		nodo_atual = Node()
		nodo_atual.data = valores[int(self.terreno[self.pos[0]][self.pos[1]])]
		nodo_atual.tag = self.pos_inicial
		nodo_atual.identifier = "%s" % self.pos_inicial
		self.arvore.add_node(nodo_atual)

		visitados = []
		relaxados = []

		fronteira = FilaPrioridade()
		fronteira.put(nodo_atual, 0)
		custo_ate_agora = {}
		custo_ate_agora[nodo_atual.identifier] = 0

		cont = 0
		encontrado = False
		while not encontrado:
			cont += 1
			u = fronteira.get()

			self.pos = u.tag
			visitados.append(self.pos)
			self.qt_tabela.item(*self.pos).setForeground(cores[1])
			self.qt_tabela.item(*self.pos).setText(QString(u"\u2022"))
			self.qt_app.processEvents()

			if self.pos == self.pos_final:
				encontrado = True
				break

			for i in range(0,4):
				pos = self.pega_posicao(i)
				if pos[0] >= 0 and pos[0] < self.max_linhas and pos[1] >= 0 and pos[1] < self.max_colunas and pos[1] not in visitados:
					peso = valores[int(self.terreno[pos[0]][pos[1]])]
					novo_peso = custo_ate_agora[u.identifier] + peso
					if ("%s" % pos) not in custo_ate_agora or novo_peso < custo_ate_agora["%s" % pos]:
						custo_ate_agora["%s" % pos] = novo_peso
						if pos not in relaxados:
							v = Node()
							v.tag = pos
							v.data = novo_peso
							v.identifier = "%s" % pos
							relaxados.append(pos)
							self.arvore.add_node(v, u.identifier)
							fronteira.put(v, novo_peso)
							self.qt_tabela.item(*v.tag).setForeground(cores[2])
							self.qt_tabela.item(*v.tag).setText(QString(u"\u25E6"))
							self.qt_app.processEvents()
						else:
							v = self.arvore.get_node("%s" % pos)
							v.data = novo_peso
							v.update_bpointer(u.identifier)
				time.sleep(0.001)
			
		final = self.arvore.get_node("%s" % self.pos_final)

		backtrack = []
		while final.bpointer:
			backtrack.append(final)
			final = self.arvore.get_node(final.bpointer)
		backtrack.append(final)

		custo = 0
		for i in reversed(backtrack):
			time.sleep(0.01)
			custo += valores[int(self.terreno[i.tag[0]][i.tag[1]])]
			self.qt_tabela.item(*i.tag).setData(self.MyBorderRole, self.pen)
			self.qt_tabela.setItemDelegate(self.delegate)
			self.qt_app.processEvents()
		final = self.arvore.get_node("%s" % self.pos_final)
		print "------------------------"
		print "- Custo: %d" % custo_ate_agora[final.identifier]
		print "- Nodos visitados: %d" % cont
		print "- Tamanho Caminho: %d" % len(backtrack)
		print "------------------------"
		

	def busca_profundidade_iterativa(self):
		profundidade = 0
		encontrado = False

		
		while not encontrado:
			cont = 0
			self.arvore = Tree()
			nodo_atual = Node()
			nodo_atual.data = self.terreno[self.pos[0]][self.pos[1]]
			nodo_atual.tag = self.pos_inicial
			nodo_atual.identifier = "%s" % self.pos_inicial

			self.arvore.add_node(nodo_atual)
			self.qt_tabela.item(*self.pos_inicial).setText(QString(u"\u2022"))
			self.qt_app.processEvents()

			visitados = []
			self.expandidos = []
			pilha = []
			pilha.append(nodo_atual)

			while pilha:
				#time.sleep(0.01)
				cont += 1
				u = pilha.pop()
				#print u.tag
				self.pos = u.tag
				visitados.append(u)
				self.qt_tabela.item(*u.tag).setForeground(cores[1])
				self.qt_tabela.item(*u.tag).setText(QString(u"\u2022"))
				self.qt_app.processEvents()
				if u.tag == self.pos_final:
					encontrado = True
					break
				self.expandir(u, profundidade)
				self.expandidos.append(u.tag)
				
				for i in reversed(u.fpointer):
					pilha.append(self.arvore.get_node(i))
				

			profundidade += 1

			#print "- %s" % visitados
			if encontrado:
				break;
			for i in visitados:
				self.qt_tabela.item(*i.tag).setText("")
				self.qt_app.processEvents()

			#self.qt_tabela = tabela_aux
			self.qt_app.processEvents()
			#time.sleep(2)
		
		final = self.arvore.get_node("%s" % self.pos_final)
		backtrack = []
		while final.bpointer:
			backtrack.append(final)
			final = self.arvore.get_node(final.bpointer)
		backtrack.append(final)

		custo = 0
		for i in reversed(backtrack):
			time.sleep(0.01)
			custo += valores[int(self.terreno[i.tag[0]][i.tag[1]])]
			self.qt_tabela.item(*i.tag).setData(self.MyBorderRole, self.pen)
			self.qt_tabela.setItemDelegate(self.delegate)
			self.qt_app.processEvents()
		print "------------------------"
		print "- Custo: %d" % custo
		print "- Nodos visitados: %d" % cont
		print "------------------------"

	def heuristica(self, a, b):
		(x1, y1) = a
		(x2, y2) = b

		return abs(x1 - x2) + abs(y1 - y2)

	def a_estrela(self):

		self.arvore = Tree()
		nodo_atual = Node()
		nodo_atual.data = valores[int(self.terreno[self.pos[0]][self.pos[1]])]
		nodo_atual.tag = self.pos_inicial
		nodo_atual.identifier = "%s" % self.pos_inicial
		self.arvore.add_node(nodo_atual)

		visitados = []
		relaxados = []

		fronteira = FilaPrioridade()
		fronteira.put(nodo_atual, 0)
		custo_ate_agora = {}
		custo_ate_agora[nodo_atual.identifier] = 0

		cont = 0
		encontrado = False
		while not encontrado:
			cont += 1
			u = fronteira.get()

			self.pos = u.tag
			visitados.append(self.pos)
			self.qt_tabela.item(*self.pos).setForeground(cores[1])
			self.qt_tabela.item(*self.pos).setText(QString(u"\u2022"))
			self.qt_app.processEvents()

			if self.pos == self.pos_final:
				encontrado = True
				break

			for i in range(0,4):
				pos = self.pega_posicao(i)
				if pos[0] >= 0 and pos[0] < self.max_linhas and pos[1] >= 0 and pos[1] < self.max_colunas and pos[1] not in visitados:
					peso = valores[int(self.terreno[pos[0]][pos[1]])]
					novo_peso = custo_ate_agora[u.identifier] + peso
					if ("%s" % pos) not in custo_ate_agora or novo_peso < custo_ate_agora["%s" % pos]:
						custo_ate_agora["%s" % pos] = novo_peso
						if pos not in relaxados:
							v = Node()
							v.tag = pos
							v.data = novo_peso
							v.identifier = "%s" % pos
							relaxados.append(pos)
							self.arvore.add_node(v, u.identifier)
							g_x = novo_peso
							h_x = self.heuristica(self.pos_final, v.tag)
							prioridade = 1 * g_x + 1.5 * h_x
							fronteira.put(v, prioridade)
							self.qt_tabela.item(*v.tag).setForeground(cores[2])
							self.qt_tabela.item(*v.tag).setText(QString(u"\u25E6"))
							self.qt_app.processEvents()
						else:
							v = self.arvore.get_node("%s" % pos)
							v.data = novo_peso
							v.update_bpointer(u.identifier)
				time.sleep(0.001)
			
		final = self.arvore.get_node("%s" % self.pos_final)

		backtrack = []
		while final.bpointer:
			backtrack.append(final)
			final = self.arvore.get_node(final.bpointer)
		backtrack.append(final)

		custo = 0
		for i in reversed(backtrack):
			time.sleep(0.01)
			custo += valores[int(self.terreno[i.tag[0]][i.tag[1]])]
			self.qt_tabela.item(*i.tag).setData(self.MyBorderRole, self.pen)
			self.qt_tabela.setItemDelegate(self.delegate)
			self.qt_app.processEvents()
		final = self.arvore.get_node("%s" % self.pos_final)
		print "------------------------"
		print "- Custo: %d" % custo_ate_agora[final.identifier]
		print "- Nodos visitados: %d" % cont
		print "- Tamanho Caminho: %d" % len(backtrack)
		print "------------------------"