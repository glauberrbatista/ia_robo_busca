
class Terreno(object):

	altura = 0
	largura = 0
	matriz = []

	def __init__(self, arq_terreno):
		super(Terreno, self).__init__()
		arquivo = open(arq_terreno, "r")
		for linha in arquivo:
			dados = linha.split()
			self.matriz.append(dados)
		self.largura = len(self.matriz)
		self.altura = len(self.matriz[0])

	def dimensoes(self):
		return self.largura, self.altura

