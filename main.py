from interface import InterfaceQt
import argparse
import json

def main():
	arquivo_config = args.input[0]
	arquivo_terreno = args.dados[0]
	tipo = args.tipo[0]

	with open(arquivo_config) as data_file:
		entrada = json.load(data_file)
	pos_inicial = [entrada['pos_inicial']['lin'],entrada['pos_inicial']['col']]
	pos_final = [entrada['pos_final']['lin'],entrada['pos_final']['col']]
	
	interface = InterfaceQt(arquivo_terreno, pos_inicial, pos_final)
	interface.executa(tipo)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Ant Clustering')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Arquivo de entrada (configuracoes)')
	parser.add_argument('-d', dest='dados', action='store', nargs=1, help='Arquivo de dados')
	parser.add_argument('-t', dest='tipo', action='store', nargs=1, help='bfs|ids|ucs|a*. BFS = Busca em Largura. IDS = Busca com aprofundamento iterativo. UCS = Busca de custo uniforme. a* = A*')

	args = parser.parse_args()

	main()