#Robo de Busca

Trabalho da disciplina de Inteligência Artificial utilizando busca cega.

Foram implementados 4 tipos de buscas distintos:

* bfs - Busca em Largura (Breadth-First Search)
* ids - Busca com Aprofundamento Iterativo (Iterative deepening depth-first search)
* ucs - Busca de Custo Uniforme (Dijkstra)
* a\*  - Busca A\*

Para executar


```
python main.py -i entrada.json -d terreno.txt -t <tipo>
```